import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule,Routes} from '@angular/router';
import { LocalizeRouterModule } from 'localize-router';


import { LoginPageComponent } from './login-page/login-page.component';


const routes :Routes = [
    { path:"", component:LoginPageComponent},

]
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
    LocalizeRouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule,
    LocalizeRouterModule
  ],
  declarations: []
})
export class AppRouteModule { }
