import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginPageComponent } from  '../login-page/login-page.component';
import { LocalizeRouterModule } from 'localize-router';

const loginRoutes: Routes = [
    { path: '', component:LoginPageComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(loginRoutes),
    LocalizeRouterModule.forChild(loginRoutes)
  ],
  exports: [ RouterModule, LocalizeRouterModule ]
})
export class LoginPageRoutingModule { }