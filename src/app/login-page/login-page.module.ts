import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginPageComponent } from  '../login-page/login-page.component';
import { TranslateModule } from '@ngx-translate/core';
import { LoginPageRoutingModule } from '../login-page/login-page-route.module';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    LoginPageRoutingModule
  ],
  declarations: [LoginPageComponent]
})
export class LoginPageModule { }