import { Component, OnInit } from '@angular/core';
import  {MdButtonModule } from '@angular/material';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  

  constructor() { }

  languages = [
    { value: 'EN', viewValue: 'EN'},
    { value: 'DE', viewValue: 'DE'}
  ];


 onSubmit(value: any){
      console.log(value);
  }
 

  ngOnInit() {
   
  }

}
