import { LocalizeUrlsPage } from './app.po';

describe('localize-urls App', () => {
  let page: LocalizeUrlsPage;

  beforeEach(() => {
    page = new LocalizeUrlsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
