# LocalizeUrls

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.3.

## Install
  Required:

    'Angular Cli' installed

  Install Angular Cli :

     > npm install -g @angular/cli


##  Porject setup:

   > git clone git@bitbucket.org:ovidiuo/part-3-url-problem.git

   > cd part-3-url-problem

   > npm install

   > ng s 

   > http://localhost:4200/


   ## Technologies 
    > Angula Material
    > Bootstrap
    > Font awesome 